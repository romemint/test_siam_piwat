# install docker
1. Install docker https://docs.docker.com/get-docker/

# install postgresql database
1. docker run --name postgresql-container -p 5432:5432 -e POSTGRES_PASSWORD=123456 -d postgres
2. docker exec -it postgresql-container psql -U postgres -c "CREATE DATABASE testdb"

# install service case clone project
1. docker build -t test-siam-piwat .
2. docker run -it -p 14046:14046 test-siam-piwat

# install service case git repositories
1. docker build -t test-siam-piwat github='https://romemint@bitbucket.org/businessdevproject/test_siam_piwat.git'
2. docker run -it -p 14046:14046 test-siam-piwat

# use docs api on
http://localhost:14046/test_siam_piwat/docs/#/

# coverage test
test_siam_piwat/coverage/index.html