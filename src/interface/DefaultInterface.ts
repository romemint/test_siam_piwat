export default interface IDefaultDao {
    findAll(): any;
    create(data: any): any;
}