import TestSiamPiwat from "./index";
import IDefaultDao from "./interface/DefaultInterface";

export default class User implements IDefaultDao {

    private testSiamPiwat: TestSiamPiwat;

    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(testSiamPiwat: TestSiamPiwat) {
        this.testSiamPiwat = testSiamPiwat;
    }

    public findAll(): any {
        return this.testSiamPiwat.db.shop.findAll({
            attributes: ["shop_id", "shop_name", "updatedAt", "createdAt"],
        }).then((shops: any) => {
            return JSON.parse(JSON.stringify(shops));
        });
    }

    public create(data: any): any {
        return this.testSiamPiwat.db.shop.create({
            shop_name: data.shop_name
        }).then(async (shop: any) => {
            console.log(">> Created shop: " + JSON.stringify(shop));
            const promises: any = [];
            for (let i = 0; i < data.table_count; i++) {
                promises.push(this.testSiamPiwat.db.table.create({
                    shop_id: shop.shop_id,
                    customer_name: null,
                    booking_id: null,
                    table_status: 'BLANK'
                }));
            }
            const resTable = await Promise.all(promises);
            console.log(">> Created table: " + JSON.stringify(resTable));
            return shop;
        });
    }
}