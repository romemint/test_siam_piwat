import TestSiamPiwat from "./index";

export default class Booking {

    private testSiamPiwat: TestSiamPiwat;

    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(testSiamPiwat: TestSiamPiwat) {
        this.testSiamPiwat = testSiamPiwat;
    }

    public async booking(data: any, id: number): Promise<any> {
        const tables = await this.testSiamPiwat.db.table.findAll({
            where: { shop_id: id, table_status: 'BLANK'}
        });
        if(tables.length === 0 || Math.ceil(data.customer_count/4) > tables.length){
            throw Error('not enough tables');
        }

        const promises: any = [];
        const bookingId = this.stringToUuid(data.customer_name);
        for (let i = 0; i < Math.ceil(data.customer_count/4); i++) {
            promises.push(this.testSiamPiwat.db.table.update({
                customer_name: data.customer_name,
                booking_id: bookingId,
                table_status: 'FULL'
            }, {
                where: { table_id: tables[i].table_id }
            }));
        }
        return {
            booking_id: bookingId
        };
    }

    public async cancel(booking_id: string): Promise<any> {
        return this.testSiamPiwat.db.table.update({
            customer_name: null,
            booking_id: null,
            table_status: 'BLANK'
        }, {
            where: { booking_id: booking_id }
        })
    }

    public stringToUuid(str: string) {
        str = str.replace('-', '');
        return 'xxxxxxxx-xxxx-4xxx-xxxx-xxxxxxxxxxxx'.replace(/[x]/g, function(c, p) {
          return str[p % str.length];
        }).toUpperCase();
    }
}