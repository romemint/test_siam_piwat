const os = require('os');
const config = require('./config/api');
process.env.NODE_ENV = config.NODE_ENV;
process.env.LISTENPORT = config.LISTENPORT;
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const {spawn} = require('child_process');
const path = require('path');

// Configure chai
chai.use(chaiHttp);
chai.should();


describe('API Server', function () {
    let server;
    this.timeout(20000);

    after(() => {
        server.kill('SIGINT');
    });

    it('should start server',async () => {

        await new Promise((resolve) => {
            server = spawn('node', [path.resolve(__dirname + '/../', 'api.js')], {env: {LISTENPORT: process.env.LISTENPORT, NODE_ENV: process.env.NODE_ENV, PATH: process.env.PATH}});
            server.stdout.on('data', function (data) {
                let line = data.toString();
                console.log('SERVER:', line);
                if (line.indexOf('port') !== -1) {
                    resolve();
                }
            });
            server.stderr.on('data', function (data) {
                process.stderr.write(data);
            });
        });
    });
});

describe('Call SDK', function () {
    const TestSiamPiwat = require('../sdk/index');
    let testSiamPiwat = new TestSiamPiwat();
    before(async () => {
        await testSiamPiwat.init('test_siam_piwat', 'TEST')
    });

    after(async () => {
        testSiamPiwat.shutdown();
    });

    it("should create shop", async() => {
        await testSiamPiwat.shop.create({
            shop_name: 'test_shop_name1',
            table_count: 5
        });
        const result = await testSiamPiwat.shop.findAll();
        expect(result).to.have.lengthOf(1);
    })

    it("should findAll shop", async() => {
        const result = await testSiamPiwat.shop.findAll();
        expect(result).to.have.lengthOf(1);
    })

    it("should create booking", async() => {
        const customerName = "stringdqweewqeqw";
        await testSiamPiwat.shop.create({
            shop_name: 'test_shop_name2',
            table_count: 5
        });
        const result = await testSiamPiwat.booking.booking({
            "customer_name": customerName,
            "customer_count": 4
        }, 1);
        expect(result.booking_id).to.equal(testSiamPiwat.booking.stringToUuid(customerName));
    })

    it("should create booking case not enough tables", async() => {
        const customerName = "stringdqweewqeqw";
        try{
            const result = await testSiamPiwat.booking.booking({
                "customer_name": customerName,
                "customer_count": 0
            }, 789987);
        }catch(error){
            expect(error.name).to.equal('Error');
        }
        
        
    })

    it("should cancel booking", async() => {
        const customerName = "stringdqweewqeqw56";
        await testSiamPiwat.shop.create({
            shop_name: 'test_shop_name3',
            table_count: 5
        });
        await testSiamPiwat.booking.booking({
            "customer_name": customerName,
            "customer_count": 2
        }, 1);
        const [ status ] = await testSiamPiwat.booking.cancel(testSiamPiwat.booking.stringToUuid(customerName));
        expect(status).to.equal(0);
    })
})