module.exports = (sequelize, DataTypes) => {
    const Table = sequelize.define("table", {
        table_id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        shop_id: {
            type: DataTypes.INTEGER,
        },
        booking_id: {
            type: DataTypes.STRING,
        },
        customer_name: {
            type: DataTypes.STRING,
        },
        table_status: {
            type: DataTypes.STRING,
        }
    });
    return Table;
};