module.exports = (sequelize, DataTypes) => {
    const Shop = sequelize.define("shop", {
        shop_id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        shop_name: {
            type: DataTypes.STRING,
            unique: true
        }
    });
    return Shop;
};