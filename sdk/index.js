"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sdk = require('@sabuytech/libsbt');
const _ = require('lodash');
const config = require('./config/' + process.env.NODE_ENV);
const db = require("../models");
const axios = require('axios');
const Shop_1 = require("./Shop");
const Booking_1 = require("./Booking");
/**
 * @class Atlantic
 * @extends Sdk
 */
class TestSiamPiwat extends Sdk {
    /**
     * @constructor
     */
    constructor() {
        super();
        this.config = config;
    }
    /**
     * Init Sale Class
     * @returns {Promise<Atlantic>}
     */
    async init(origin, facility) {
        await super.init(origin, facility, false);
        this.db = db;
        this.shop = await new Shop_1.default(this);
        this.booking = await new Booking_1.default(this);
        this.otherApi = axios.create({
            baseURL: config.BOOK_PUBLISHER_PATH,
        });
        return this;
    }
    shutdown() {
        super.shutdown();
    }
}
exports.default = TestSiamPiwat;
module.exports = TestSiamPiwat;
//# sourceMappingURL=index.js.map