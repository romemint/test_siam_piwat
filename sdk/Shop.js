"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class User {
    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(testSiamPiwat) {
        this.testSiamPiwat = testSiamPiwat;
    }
    findAll() {
        return this.testSiamPiwat.db.shop.findAll({
            attributes: ["shop_id", "shop_name", "updatedAt", "createdAt"],
        }).then((shops) => {
            return JSON.parse(JSON.stringify(shops));
        });
    }
    create(data) {
        return this.testSiamPiwat.db.shop.create({
            shop_name: data.shop_name
        }).then(async (shop) => {
            console.log(">> Created shop: " + JSON.stringify(shop));
            const promises = [];
            for (let i = 0; i < data.table_count; i++) {
                promises.push(this.testSiamPiwat.db.table.create({
                    shop_id: shop.shop_id,
                    customer_name: null,
                    booking_id: null,
                    table_status: 'BLANK'
                }));
            }
            const resTable = await Promise.all(promises);
            console.log(">> Created table: " + JSON.stringify(resTable));
            return shop;
        });
    }
}
exports.default = User;
//# sourceMappingURL=Shop.js.map