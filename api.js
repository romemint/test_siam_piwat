const cors = require('cors');
const path = require('path');
const TestSiamPiwat = require('./sdk');
const LISTENPORT = process.env.LISTENPORT;
const LANGUAGE = 'th-TH';
module.exports = (
    /**
     * @param {Auth} ATLAS
     * @param {TestSiamPiwat} TESTSIAMPIWAT
     * @returns {Promise<express>}
     */
    async (TESTSIAMPIWAT) => {
        try {
            global.TESTSIAMPIWAT = TESTSIAMPIWAT;
            global.LOG_DB = false;

            process.on('SIGINT', function () {
                TESTSIAMPIWAT.shutdown();
                process.exit();
            });
            TESTSIAMPIWAT.setElasticSearchLog(TESTSIAMPIWAT.config.elasticsearch);
            await TESTSIAMPIWAT.init('test_siam_piwat', 'api');

            const options = {
                controllers: path.join(__dirname, './api'),
                loglevel: 'error',
                strict: true,
                router: true,
                validator: true,
                customErrorHandling: true,
                port: LISTENPORT,
                docs: {
                    apiDocs: '/api-doc',
                    apiDocsPrefix: '/test_siam_piwat',
                    swaggerUi: '/docs',
                    swaggerUiPrefix: '/test_siam_piwat',
                },
                expressJson: {
                    limit: '2mb'
                }
            };

            const app = await TESTSIAMPIWAT.initApi(__dirname + '/api.yaml', options);

            //add default language as query param to each request
            app.use((req, res, next) => {
                req.language = req.query.language || LANGUAGE;
                next();
            });

            app.use(cors());

            return await TESTSIAMPIWAT.setApiRoutes();
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    }
)(new TestSiamPiwat());

