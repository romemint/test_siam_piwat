module.exports.booking = async (req, res, next) => {
    try {
        const result = await TESTSIAMPIWAT.booking.booking(req.body, req.params.shop_id);
        res.status(200).send({
            result: result
        });
    } catch (e) {
        next(e)
    }
};
module.exports.cancel = async (req, res, next) => {
    try {
        const [ status ] = await TESTSIAMPIWAT.booking.cancel(req.params.booking_id);
        if(status){
            res.status(200).send();
        }else{
            throw Error('booking id not found');
        }
    } catch (e) {
        next(e)
    }
};